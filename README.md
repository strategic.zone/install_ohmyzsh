# How to deploy Oh-My-Zsh
Orig: https://github.com/ohmyzsh/ohmyzsh
On linux systems deploy with:
```
sh <(curl --silent https://gitlab.com/strategic.zone/install_ohmyzsh/raw/master/install_ohmyzsh.sh)
```
Also, you can later add more plugins => https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins